import { SharedService } from './../services/shared.service';
import { User } from './../model/user';
import { logInData } from './../login-popup/logInData';
import { DialogOverviewDialog } from './../login-popup/popup.component';
import { CookieService } from 'ngx-cookie-service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject, Input } from '@angular/core'; 
import { Subscription } from 'rxjs';

@Component({
  selector: 'naviation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit { 
  user: User;
  signInStatus : boolean;
  clickEventsubscription:Subscription;

  constructor(public dialog: MatDialog,  private cookieService: CookieService, private sharedService:SharedService) {
    this.clickEventsubscription = this.sharedService.getEvent().subscribe(user=>{ //get event from popup.component.ts
      console.log("subscribe: ", JSON.stringify(user[0])); 
      this.cookieService.set("user_info_ks", JSON.stringify(user[0]));
      this.closeLogin();
    })
  } 
   
  ngOnInit(): void {    
    this.closeLogin(); 
  } 

  signOutFun(): void{
    if(this.cookieService.get('username_ks') != ""){
      this.signInStatus = false;
      this.cookieService.set("username_ks", "");
      this.cookieService.set("password_ks", ""); 
      this.cookieService.set("user_info_ks", "");
      this.openDialog();  
    }   
  }

  signInFun(): void{
    if(this.cookieService.get('username_ks') == ""){
      this.openDialog(); 
    } 
  }

  openDialog(): void {
    var usn = this.cookieService.get('username_ks');
    var psw = this.cookieService.get('password_ks');
    if(usn == '' && psw == ''){
      const dialogRef = this.dialog.open(DialogOverviewDialog, {
        width: '280px',
        // data: {password: this.password, username: this.username} 
      }); 

      // dialogRef.afterClosed().subscribe(() =>{
      //   this.closeLogin(); 
      // }); 
    } 
  }

  closeLogin(): void{
    if(this.cookieService.get('username_ks') != ""){
      this.signInStatus = true;

      console.log("JSONL: ",this.cookieService.get('user_info_ks'));
      this.user = JSON.parse(this.cookieService.get('user_info_ks'));
    }else{
      this.signInStatus = false;
    }  
  }

} 