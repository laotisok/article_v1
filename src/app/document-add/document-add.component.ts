import { File as Files } from './../model/file';
import { Subscription } from 'rxjs';
import { SharedService } from './../services/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Document } from './../model/document';
import { DocumentService } from './../services/document.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { FormGroup, NgForm } from '@angular/forms';
import { type } from 'os';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs'; 
import { NgbProgressbar } from '@ng-bootstrap/ng-bootstrap'; 
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
interface Category {
  category_id: string;
  category_name: string;
}

interface Type {
  type_id: string;
  type_name: string;
}

export class DocAndFile {
  document:Document
  file:Files[]
}

@Component({
  selector: 'document-add',
  templateUrl: './document-add.component.html',
  styleUrls: ['./document-add.component.css']
})
export class DocumentAddComponent implements OnInit {
  @ViewChild('addForm', {static: false}) addForm: NgForm;

  @ViewChild('fileInput') fileInput: any;

  public Editor = ClassicEditor; 
  documents: Document[];
  file: Files;
  clickEventsubscription:Subscription; 
  categorySt=false;
  categorySelect: any; 
  userId: number;
  removeImg: string[];

  data: DocAndFile;
  loading= false;

  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  show = false; 
  fileInfos: Observable<any>; 
  files = []; 

  categorys: Category[] = [
    { category_id: '0', category_name: '' },
    { category_id: '01', category_name: 'General'}, 
  ];

  constructor(private route: ActivatedRoute, private documentService: DocumentService, private sharedService:SharedService) {
    this.clickEventsubscription = this.sharedService.getCategory().subscribe(category=>{ //get event from popup.component.ts
      console.log("title: ", category);    
      var category_ = {category_id:Date.now().toString(), category_name:category["category_name"]};
      this.categorys.push(category_);
      this.categorySt=true; 
      this.categorySelect = category_;  
      console.log("categorySelect: ",this.categorySelect);
      console.log("categorys: ",this.categorys);
    });

    this.sharedService.getListCategory().subscribe(listCategory=>{
      console.log("listCategory: ",listCategory);
      var array = this.categorys;
      listCategory.forEach(function(n){
        var d = {category_id:n["doc_category_id"], category_name:n["doc_category_name"]}; 
        array.push(d); 
      })  
      this.categorys = array;
      console.log("array: ",array); 
    });
  } 

  types: Type[] = [
    { type_id: '0', type_name: ' ' },
    { type_id: '1', type_name: 'Programming' },
    { type_id: '2', type_name: 'Technology' }, 
    { type_id: '3', type_name: 'Philosophy' },
    { type_id: '4', type_name: 'Science' },
    { type_id: '5', type_name: 'History' },
    { type_id: '6', type_name: 'Fiction' },
    { type_id: '7', type_name: 'Sci-fi' },
    { type_id: '8', type_name: 'Comedy' },
    { type_id: '9', type_name: 'Other' },
  ];


  ngOnInit(): void {   
    this.removeImg = [];
    this.route.paramMap.subscribe(params => {
      this.userId = +params.get('id');
    });   

    // this.fileInfos = this.documentService.getFiles();
    // console.log("ngOnInit this.fileInfos: ",this.documentService.getFiles());
  }

  // getMyCategory(){
  //   this.documentService.findAllMyCategory(this.userId).then(categories => {
  //     console.log("Success: ",categories); 
  //   })
  // }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement(),
    );
  } 

  addDoc(doc){  //add doc and files
    console.log("Doc: ",doc);  
    this.loading = true;
    try{
      var date_ = new Date(); 
      var date = this._formatDatetime(date_, 'dd-mm-yyyy');  
      
      this.categorySt == true? doc.doc_category_new = true : doc.doc_category_new = false; 

      doc.doc_created_date = date;
      doc.user_id    = this.userId;  
      doc.doc_id = Date.now().toString(); 

      var files_ = [];
      this.files.forEach(element => {  //doc["doc_file"].
        console.log("FILES: ",element);
        var file = {id:'', doc_file_name:element.name, doc_file_path:"https://storage.googleapis.com/mydocfile/"+element.name, doc_id:doc.doc_id, doc_file_created_date:date, doc_file_status:''};
        files_.push(file);
      });
      console.log("file: ",files_); 

      doc.doc_category_id = doc.doc_category.category_id;
      doc.doc_category_name = doc.doc_category.category_name; 

      this.data = {document:doc, file:files_}; 
      console.log("addDoc: ",this.data); 
      if(doc.doc_category.category_id != null && doc.doc_type_name != null && doc.doc_name != null && doc.doc_content != null && doc.doc_category.category_id != "" && doc.doc_type_name != "" && doc.doc_name != "" && doc.doc_content != ""){
        this.documentService.addDocument(this.data).then( DocAndFile => {
          //this.data = document; 
          this.loading = false; 
          this.message = '';
          this.fileInfos = new Observable<any>();
          this.addForm.resetForm(); //remove data from fields   
          this.fileInput.nativeElement.value = '';  //remove input type file value
          console.log("SUCCESS: ",DocAndFile);  
        }, err => { 
          this.loading = false;
          console.log("ERROR: ",err); 
        })
      }else{
        this.loading = false;
        console.log("ERROR"); 
      }
    }catch(e){ this.loading = false; }
  }

  _formatDatetime(date: Date, format: string) {
    const _padStart = (value: number): string => value.toString().padStart(2, '0');
    return format
     .replace(/yyyy/g, _padStart(date.getFullYear()))
     .replace(/dd/g, _padStart(date.getDate()))
     .replace(/mm/g, _padStart(date.getMonth() + 1))
     .replace(/hh/g, _padStart(date.getHours()))
     .replace(/ii/g, _padStart(date.getMinutes()))
     .replace(/ss/g, _padStart(date.getSeconds()));
  }

  removeFile(filename){
    console.log("filename: ",filename);
    this.removeImg.push(filename);
    var newFile = [];
    var i = 0;
    this.fileInfos.subscribe(d=>{
      newFile = d;
      console.log("d: ",d)
      this.removeImg.forEach(name => { 
        newFile.forEach(fn=>{
          if(name == fn.name){ 
            console.log("i: ",i);
            
            newFile.splice(i, 1);
          }  
          i++;
        })    
        i=0; 
     }); 
      this.files = newFile; 
    }) 
  }

  hasClass(filename){
    var status = false;
    if(this.removeImg.length >0){
      for(let cl of this.removeImg ){ 
        if(cl == filename){
          status = true;
        }
      }
    }  
    return status;
  }

  // Upload files ** 

  selectFile(event) { 
    this.selectedFiles = event.target.files;  
    this.upload();
  }

  upload() {
    this.progress = 0; 

    this.currentFile = this.selectedFiles.item(0);  
    for(var i=0; i<=this.selectedFiles.length; i++){
      this.documentService.upload(this.selectedFiles.item(i)).subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.message = event.body.message;
            this.fileInfos = this.documentService.getFiles(); 
            this.fileInfos.subscribe(d=>{
              this.files = d;  
            }); 
            this.show = true;
          }
        },
        err => {
          this.progress = 0;
          //this.message = 'Could not upload the file!';
          this.currentFile = undefined;
        });
    } 
    this.selectedFiles = undefined;  
    
  }
  // Upload files **

  reset(f){
    f.value = "";
  }

}
