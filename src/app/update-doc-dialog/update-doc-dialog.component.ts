import { ActivatedRoute } from '@angular/router';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { Document } from './../model/document';
import { File as Files } from './../model/file';
import { DocumentService } from './../services/document.service';
import { SharedService } from '../services/shared.service';
import { Component, Inject, Input } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { $ } from 'protractor';


export interface DocData {
  docId: string;
  userId: string;
}

interface Category {
  category_id: string;
  category_name: string;
}

interface Type {
  type_id: string;
  type_name: string;
}

export class DocAndFile {
  document:Document
  file:Files[]
}

@Component({
  selector: 'update-doc-dialog',
  templateUrl: './update-doc-dialog.component.html',
  styleUrls: ['./update-doc-dialog.component.css']
})
export class UpdateDocDialogComponent {
  @Input() public docId: string;
  title_name: string; 
  userId: string;
  constructor(public dialog: MatDialog, private route: ActivatedRoute) {
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
    });   
  }

  openDialog(): void { 
    const dialogRef = this.dialog.open(docDialog, {
      width: '60%',
      height: '83%', 
      data: {docId: this.docId, userId: this.userId}
    }); 
  } 

}

@Component({
  selector: 'doc-dialog',
  templateUrl: './doc-dialog.html',
  styleUrls: ['./update-doc-dialog.component.css']
})
export class docDialog {
  docAndFile: DocAndFile;
  public Editor = ClassicEditor; 
  categorySelect: any;  
  docId: string;
  userId: string;
  documents: Document;
  files_: Files[]; 
  categorySt=false;
  loading= false;

  removeImg: string[];
  selectedFiles: FileList;
  currentFile: File;
  progress = 0;
  message = '';
  show = false; 
  fileInfos: Observable<any>; 
  files = []; 
  tmpFiles = []; 
  newFile = []; 

  clickEventsubscription:Subscription;

  categorys: Category[] = [
    { category_id: '0', category_name: '' },
    { category_id: '01', category_name: 'General'}, 
  ];

  types: Type[] = [
    { type_id: '0', type_name: ' ' },
    { type_id: '1', type_name: 'Programming' },
    { type_id: '2', type_name: 'Technology' }, 
    { type_id: '3', type_name: 'Philosophy' },
    { type_id: '4', type_name: 'Science' },
    { type_id: '5', type_name: 'History' },
    { type_id: '6', type_name: 'Fiction' },
    { type_id: '7', type_name: 'Sci-fi' },
    { type_id: '8', type_name: 'Comedy' },
    { type_id: '9', type_name: 'Other' },
  ];

  constructor(private route: ActivatedRoute,
    public dialogRef: MatDialogRef<docDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DocData, private sharedService: SharedService, private documentService: DocumentService) {
      console.log("docId 2: ",data);
      this.docId = data['docId']; 
      this.userId = data['userId']; 
      this.removeImg = [];

      // this.route.paramMap.subscribe(params => {
      //   this.userId = +params.get('id');
      //   console.log("this.userId: ",this.userId);
      // });   

      // Category ***
      this.clickEventsubscription = this.sharedService.getCategory().subscribe(category=>{ //get event from popup.component.ts  
        var category_ = {category_id:Date.now().toString(), category_name:category["category_name"]};
        this.categorys.push(category_);
        this.categorySt=true; 
        this.categorySelect = category_;  
        this.documents.doc_category_id = +category_.category_id;
        this.documents.doc_category_name = category_.category_name;  

        console.log("categorySelect 2: ", this.categorySelect);   
        
      });
  
      this.sharedService.getListCategory().subscribe(listCategory=>{
        console.log("listCategory: ",listCategory);
        var array = this.categorys;
        listCategory.forEach(function(n){
          var d = {category_id:n["doc_category_id"], category_name:n["doc_category_name"]}; 
          array.push(d); 
        })  
        this.categorys = array; 
      });
      // Category ***

      this.getData();
  }

  getData(){
    this.loading= true;
    this.documentService.findOneUpdate(Number(this.docId),this.userId)
        .subscribe(documents => { 
          console.log("documents: ",documents); 

          this.documents = documents["data"][0];  
          this.files_ = documents["file"]; 
          this.tmpFiles = documents["file"]; 
          this.newFile = documents["file"];  
          var doc_type = this.documents["doc_type_name"];  

          this.types.forEach(d=>{ 
            if(doc_type == d.type_name){
              this.documents["doc_type_id"] = d.type_id;
            }
          });
          this.loading = false;

          documents["categories"].forEach(element => {
            var ctg = {category_id: element["doc_category_id"].toString(), category_name: element["doc_category_name"]};
            this.categorys.push(ctg);
          });

          var category_ = {category_id: documents["data"][0]["doc_category_id"].toString(), category_name:documents["data"][0]["doc_category_name"]};
          this.categorySelect = category_; 

          console.log("categorySelect: ",this.categorySelect);
          console.log("categorys     : ",this.categorys);
        });  
  }

  public onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement(),
    );
  } 

  onNoClick(): void {
    this.dialogRef.close();
  }
  getCategory(category){
    // var titleId = Date.now() 
    this.sharedService.setCategory(category);
  }

  updateDoc(doc){ 
    console.log("updateDoc: ",doc);
    
    this.loading = true;
    doc.user_id    = this.userId;  
    doc.doc_id     = this.docId ;  

    var date_ = new Date(); 
    var date = this._formatDatetime(date_, 'dd-mm-yyyy');  
    
    this.categorySt == true? doc.doc_category_new = true : doc.doc_category_new = false; 

    doc.doc_created_date = date;

    //get doc_type_name through doc_type_id
    var doc_type = doc.doc_type_id 
    this.types.forEach(d=>{ 
      if(doc_type == d.type_id){
        doc.doc_type_name = d.type_name;
        delete doc.doc_type_id;
      }
    }); 

    if(doc.doc_file == undefined){
      doc.doc_file = [];
    }
    var file = doc.doc_file;  

    // doc.doc_category_id = doc.doc_category["category_id"];
    // doc.doc_category_name = doc.doc_category["category_name"];

    var doc_type_id = doc.doc_category_id 
    this.categorys.forEach(d=>{ 
      if(doc_type_id == d.category_id){
        doc.doc_category_name = d.category_name; 
      }
    }); 

    this.docAndFile = {document:doc, file:file};

    // if(doc.category_id != null && doc.doc_type_name != null && doc.doc_name != null && doc.doc_content != null && doc.doc_category.category_id != "" && doc.doc_type_name != "" && doc.doc_name != "" && doc.doc_content != ""){
      this.documentService.updateDocument(this.docAndFile).then( DocAndFile => {
        //this.data = document; 
        this.loading = false;
        console.log("SUCCESS: ",DocAndFile); 
      }, err => { 
        this.loading = false;
        console.log("ERROR: ",err); 
      })
    // }else{
    //   this.loading = false;
    //   console.log("ERROR"); 
    // }

    console.log("updateDoc data: ",doc);
    console.log("updateDoc docAndFile: ",this.docAndFile);
  }

  removeFile(filename){
    console.log("filename: ",filename);
    this.removeImg.push(filename);
    // var newFile = [];
    var i = 0;  
    this.removeImg.forEach(name => { 
      this.newFile.forEach(fn=>{
        if(name == fn.doc_file_name){ 
          console.log("i: ",i);
          
          this.newFile.splice(i, 1);
        }  
        i++;
      })    
      i=0;  
      this.files_ = this.newFile; 
    }) 
    console.log("this.files_: ",this.files_);
    
  }

  hasClass(filename){
    var status = false;
    if(this.removeImg.length >0){
      for(let cl of this.removeImg ){ 
        if(cl == filename){
          status = true;
        }
      }
    }  
    return status;
  }

  // Upload files ** 

  selectFile(event) { 
    this.selectedFiles = event.target.files;  
    this.upload();
  }

  upload() {   

    console.log("newFile: ",this.newFile);
    this.progress = 0;
    this.currentFile = this.selectedFiles.item(0);  
    for(var i=0; i<=this.selectedFiles.length; i++){
      this.documentService.upload(this.selectedFiles.item(i)).subscribe(
        event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            // this.files_=this.tmpFiles;
            this.message = event.body.message;
            this.fileInfos = this.documentService.getFiles(); 
            var fi = [];
            this.fileInfos.subscribe(d=>{ 
              console.log("d: ",d);  
              fi = d;  
              var i = 0;
              this.files_.forEach(element => {
                console.log("element: ",element);  
                d.forEach(e=>{
                  if(e.name == element.doc_file_name){
                    fi.splice(i, 1); 
                    console.log("fi: ",fi);  
                  }
                  i++;
                })
                i = 0; 
              });

              fi.forEach(element=>{
                var date_ = new Date(); 
                var date = this._formatDatetime(date_, 'dd-mm-yyyy');    

                var f = {id:'', doc_file_name:element.name, doc_file_path:"https://storage.googleapis.com/mydocfile/"+element.name, doc_id:this.docId.toString(), doc_file_created_date:date, doc_file_status:''};
                this.files_.push(f); //add new file to files that get from server

              })

            }); 
            this.show = true;
          }
        },
        err => {
          this.progress = 0;
          this.message = 'Could not upload the file!';
          this.currentFile = undefined;
        });
    } 
    this.selectedFiles = undefined;   
    console.log("this.tmpFile: ",this.tmpFiles);
    console.log("this.files_: ",this.files_);
    
  }
  // Upload files **

  _formatDatetime(date: Date, format: string) {
    const _padStart = (value: number): string => value.toString().padStart(2, '0');
    return format
     .replace(/yyyy/g, _padStart(date.getFullYear()))
     .replace(/dd/g, _padStart(date.getDate()))
     .replace(/mm/g, _padStart(date.getMonth() + 1))
     .replace(/hh/g, _padStart(date.getHours()))
     .replace(/ii/g, _padStart(date.getMinutes()))
     .replace(/ss/g, _padStart(date.getSeconds()));
  }

}
