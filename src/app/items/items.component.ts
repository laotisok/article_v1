import { SharedService } from './../services/shared.service';
import { File } from './../model/file';
import { Document } from './../model/document';
import { DocumentService } from './../services/document.service';
import { Component, OnInit } from '@angular/core'; 

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  documents: Document[]; 
  files: File[];
  loading= false;

  constructor(private documentService: DocumentService, private sharedService: SharedService) {
    this.sharedService.getFilter().subscribe(response => { //get filter value from search.component.ts
      this.loading= true;
      this.documentService.getFilter(response).subscribe(documents=>{ 
        this.documents = documents["data"]
        this.files = documents["file"];  
        console.log("documents: ",documents);
        
        var i = 0;
        this.documents.forEach(d=>{ 
          this.files.forEach(f=>{   
            if(Number(f.doc_id) == Number(d.doc_id)){
              this.documents[i]["file"] = f["doc_file_path"];
            }
          });  
          i++;
        })
        this.loading= false;
      })
    }) 

    this.sharedService.getSearch().subscribe(response => { //get search value from search.component.ts
      this.loading= true;
      this.documentService.getSearch(response).subscribe(documents=>{  
        console.log("Search Data: ",documents);
        
        this.documents = documents["data"];
        this.files = documents["file"];  

        var i = 0;
        this.documents.forEach(d=>{ 
          this.files.forEach(f=>{   
            if(Number(f.doc_id) == Number(d.doc_id)){
              this.documents[i]["file"] = f["doc_file_path"];
            }
          });  
          i++;
        })
        this.loading= false;
      })
    }) 
  }

  ngOnInit(): void {
    this.getAllDoc();
  }

  getAllDoc(){
    this.loading= true;
    this.documentService.findAll().then(
      documents =>{
        this.documents = documents["data"]
        this.files = documents["file"];  

        var i = 0; 
        this.documents.forEach(d=>{ 
          this.files.forEach(f=>{
            if(Number(f.doc_id) == Number(d.doc_id)){
              if(f["doc_file_path"] == "" || f["doc_file_path"] == "unknown"){
                this.documents[i]["file"] = String.raw`assets\no-img.png`;
              }else{
                this.documents[i]["file"] = f["doc_file_path"];
              }
            }else{
              this.documents[i]["file"] = String.raw`assets\no-img.png`;
            }
          });  
          i++;
        })
        this.loading= false;
        console.log("Document: ",this.documents);
        console.log("File: ",this.files);
      },
      err =>{ console.log("ERROR: ",err); }
    );
  }

}
