import { Category } from './../model/category';
import { Document } from './../model/document';
import { User } from './../model/user';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
providedIn: 'root'
})
export class SharedService {
  private subject      = new Subject<any>();
  private category     = new Subject<any>();
  private listCategory = new Subject<any>();
  private filterName   = new Subject<string>();
  private searchVal   = new Subject<string>();
  user: User;
 
  sendEvent(user:User) { 
    console.log("User: ",user); 
    this.subject.next(user);
  }
  getEvent(): Observable<any>{  
    return this.subject.asObservable();
  }

  setCategory(category: string){
    console.log("Title: ",category);
    this.category.next(category); 
  }

  getCategory(): Observable<any>{  
    return this.category.asObservable();
  }

  setListCategory(categories: Category[]){
    console.log("listCategory 1: ",categories); 
    this.listCategory.next(categories);
  }

  getListCategory(): Observable<any>{
    return this.listCategory.asObservable();
  }

  setFilter(filterName: string){
    console.log("setFilter: ",filterName);
    this.filterName.next(filterName);
  }

  getFilter(): Observable<string>{
    return this.filterName.asObservable();
  }

  setSearch(searchVal: string){
    console.log("setFilter: ",searchVal);
    this.searchVal.next(searchVal);
  }

  getSearch(): Observable<string>{
    return this.searchVal.asObservable();
  }

}