import { DocAndFile } from './../document-add/document-add.component';
import { Document } from './../model/document';
import { Http,Headers } from '@angular/http';
import { Injectable } from '@angular/core'; 
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import { Category } from '../model/category';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { HttpClient, HttpRequest, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {
  private apiUrl = '/api/documents'; 
  constructor(private http: Http, private http_: HttpClient) { }  

  findAll(): Promise<Array<Document>>{ //all data
    return this.http.get(this.apiUrl)
      .toPromise()
      .then(response => response.json() as Document[])
      .catch(this.handleError);
  }

  findAllMine(userId: number): Promise<Array<Document>>{ //all data of profile
    return this.http.get(this.apiUrl+'/profile/'+userId)
      .toPromise()
      .then(response => response.json() as Document[])
      .catch(this.handleError);
  }

  findAllMyCategory(userId: number): Promise<Array<Category>>{
    return this.http.get(this.apiUrl+'/prifile/'+userId)
            .toPromise()
            .then(response => response.json() as Category[])
            .catch(this.handleError);
  }

  findOne(docId: number){  //for detail page
    return this.http.get(this.apiUrl+'/'+docId) 
    .map(response => response.json())
    .catch(this.handleError);
  }

  findOneUpdate(docId: number, userId: string){  //for update page
    return this.http.get(this.apiUrl+'/docId/'+docId+'/userId/'+userId) 
    .map(response => response.json())
    .catch(this.handleError);
  } 

  addDocument(docAndFile: DocAndFile):Promise<Array<DocAndFile>>{
    console.log("docAndFile: ",docAndFile); 
    let docHeaders = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.apiUrl, JSON.stringify(docAndFile), { headers: docHeaders})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }

  updateDocument(docAndFile: DocAndFile):Promise<Array<DocAndFile>>{
    let docHeaders = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.apiUrl+'/update', JSON.stringify(docAndFile), { headers: docHeaders})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
  }

  deleteDocument(docId: string){
    return this.http.get(this.apiUrl+'/delete/'+docId)
      .map(response => response.toString())
      .catch(this.handleError);
  } 

  private handleError(error: any): Promise<Array<any>> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  signIn(username: string, password: string){
    return this.http.get(this.apiUrl+'/login/'+username+'/'+password).map(Response => Response.json()).catch(this.handleError);
  }

  // upload file **
  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    console.log("upload: ",file);
    formData.append('file', file);

    const req = new HttpRequest('POST', `${this.apiUrl}/file/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http_.request(req);
  }

  getFiles(): Observable<any> {
    return this.http_.get(`${this.apiUrl}/file/files`);
  }
  // upload file **

  getFilter(filterName: string){
    return this.http.get(this.apiUrl+'/filter/'+filterName).map(Response => Response.json()).catch(this.handleError);
  }

  getSearch(searchVal: string){
    return this.http.get(this.apiUrl+'/search/'+searchVal).map(Response => Response.json()).catch(this.handleError);
  }
  
} 