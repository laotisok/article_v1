import { Document } from './../model/document';
import { DocumentService } from './../services/document.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'items-detail',
  templateUrl: './items-detail.component.html',
  styleUrls: ['./items-detail.component.css']
})
export class ItemsDetailComponent implements OnInit {
  documents: Document; 
  files: File;
  loading= false;
  constructor(private route: ActivatedRoute, private router: Router, private documentService: DocumentService) { }

  ngOnInit() {  //get id and find data
    this.loading= true;
    this.route.paramMap.subscribe(params => {
      let docId = +params.get('id');  //+ mean convert to Number
      console.log("ID: ",docId);
      this.documentService.findOne(docId)
        .subscribe(documents => { 
          this.documents = documents["data"][0];  
          this.files = documents["file"];
          this.loading= false;
          console.log("document FIND ONE: ",this.documents)  
          console.log("file FIND ONE: ",this.files)  
        });
    })
  } 
}
