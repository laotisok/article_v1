import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  animal: 'panda' | 'unicorn' | 'lion';
}

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  showFiller = false;
  show = 1; 
  username: string;
  signInStatus : boolean;

  dashboard = 1;
  constructor(public dialog: MatDialog,private route: ActivatedRoute, private cookieService: CookieService, private router: Router) {
    this.route.paramMap.subscribe(params => {
      this.username = params.get('username');
      console.log("username: ",this.username);
    });  
  }

  // openDialog() {
  //   const dialogRef = this.dialog.open(DialogDataDialog, {
  //     data: {
  //       animal: 'panda'
  //     }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed'); 
  //   });
  // }

  ngOnInit(): void {
    this.route.queryParamMap
      .subscribe(params => {
        let param = +params.get('tab');
        this.dashboard = param;   
      })
  }

  toggleSidebar(drawer){   
    drawer.toggle();
    this.show == 1?this.show = 0: this.show = 1; 
  }

  sidebarTab(index){
    if(index == 1){ 
      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { tab:'1' }});  
      this.dashboard = 1; 
    }
    if(index == 2){ 
      this.router.navigate(['.'], { relativeTo: this.route, queryParams: { tab:'2' }});  
      this.dashboard = 2; 
    }
  }

  signOutFun(): void{
    if(this.cookieService.get('username_ks') != ""){
      this.signInStatus = false;
      this.cookieService.set("username_ks", "");
      this.cookieService.set("password_ks", ""); 
      this.cookieService.set("user_info_ks", "");
      // this.openDialog();  
    }   
  }

}

@Component({
  selector: 'note-dialog',
  templateUrl: './note-dialog.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class DialogDataDialog {
  constructor(public dialogRef: MatDialogRef<DialogDataDialog>,@Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
