import { SharedService } from '../services/shared.service';
import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface DialogData {
  title_id: string;
  title_name: string;
}

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'add-category-dialog',
  templateUrl: './add-category-dialog.component.html',
  styleUrls: ['./add-category-dialog.component.css']
})
export class AddCategoryDialogComponent {

  title_name: string; 

  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(CategoryDialog, {
      width: '250px',
      data: {title_name: this.title_name}
    });

    // dialogRef.afterClosed().subscribe(result => {
    //   console.log('The dialog was closed: ',result);
    //   this.title_name = result;
    // });
  }

}

@Component({
  selector: 'category-dialog',
  templateUrl: './category-dialog.html',
  styleUrls: ['./add-category-dialog.component.css']
})
export class CategoryDialog {

  constructor(
    public dialogRef: MatDialogRef<CategoryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private sharedService: SharedService) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  getCategory(category){
    // var titleId = Date.now() 
    this.sharedService.setCategory(category);
  }

}
