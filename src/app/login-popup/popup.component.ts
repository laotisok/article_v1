import { SharedService } from './../services/shared.service';
import { User } from './../model/user';
import { DocumentService } from './../services/document.service';
import { logInData } from './logInData';
import { Component, Inject, OnInit, Input, Output } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';   
import { EventEmitter } from 'protractor';

@Component({
  selector: 'popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class LoginDialogComponent implements OnInit{

  username: string;
  password: string;  

  // invalidLogin: boolean; 

  constructor(public dialog: MatDialog, private cookieService: CookieService) {}
  ngOnInit(): void {  
      this.openDialog(); 
  }

  openDialog(): void {
    var usn = this.cookieService.get('username_ks');
    var psw = this.cookieService.get('password_ks');
    if(usn != 'laoti' && psw != '123'){
      const dialogRef = this.dialog.open(DialogOverviewDialog, {
        width: '280px',
        data: {password: this.password, username: this.username} 
      }); 
    } 
  }

  // logOut(): void{
  //   if(this.cookieService.get('username_ks') != ""){
  //     this.cookieService.set("username_ks", "");
  //     this.cookieService.set("password_ks", "");
  //     this.openDialog();
  //   }  
  // }

}

@Component({
  selector: 'login-dialog',
  templateUrl: './login-dialog.html',
  styleUrls: ['./popup.component.css']
})
export class DialogOverviewDialog {
  login_msg : string;  
  user: User;  
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewDialog>,private documentService: DocumentService,
    @Inject(MAT_DIALOG_DATA) public data: logInData, private cookieService: CookieService,private sharedService:SharedService) {}

  onNoClick(): void {  
    this.dialogRef.close();
  } 

  signIn(credentials) {
    console.log("signIn: ",credentials);
      var usn = credentials["username"];
      var psw = credentials["password"];
      this.documentService.signIn(usn,psw).subscribe(user => {
        this.user = user; 
        this.user["id"];
        console.log("USER: ",user); 
        if(user.length == 1){  
          this.dialogRef.close();
          this.cookieService.set("username_ks", usn);
          this.cookieService.set("password_ks", psw); 
          this.sharedService.sendEvent(user); //send event to navigation.component.ts
        }else{
          this.login_msg = "Faild to Login!";
        }
      });  
  }

}
