import { HomepageComponent } from './homepage/homepage.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';  
import { MaterialModule } from './material-module';
import { CarouselComponent } from './carousel/carousel.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ItemsComponent } from './items/items.component';
import { LoginComponent } from './login/login.component';  
import { HttpModule } from '@angular/http';
import { SearchComponent } from './search/search.component';
import { ItemsDetailComponent } from './items-detail/items-detail.component';
import { SidebarComponent,DialogDataDialog } from './sidebar/sidebar.component';
import { LoginDialogComponent, DialogOverviewDialog } from './login-popup/popup.component';  
import { CookieService } from 'ngx-cookie-service';
import { ProfileComponent } from './profile/profile.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { DocumentAddComponent, DocAndFile } from './document-add/document-add.component'; 

import { CKEditorModule } from '@ckeditor/ckeditor5-angular'; 
import { AddCategoryDialogComponent, CategoryDialog } from './add-category-dialog/add-category-dialog.component';
import { UpdateDocDialogComponent, docDialog } from './update-doc-dialog/update-doc-dialog.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent, 
    CarouselComponent,
    ItemsComponent,
    LoginComponent,
    HomepageComponent,
    SearchComponent,
    ItemsDetailComponent,
    SidebarComponent, 
    DialogDataDialog, 
    LoginDialogComponent,
    DialogOverviewDialog,
    ProfileComponent,
    AdminDashboardComponent,
    DocumentAddComponent, 
    AddCategoryDialogComponent,
    CategoryDialog,
    UpdateDocDialogComponent, 
    docDialog,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    MaterialModule, 
    NgbModule,
    HttpModule,
    HttpClientModule,
    CKEditorModule,
    RouterModule.forRoot([ 
      { path: '', component: HomepageComponent },
      { path: 'login', component: LoginComponent },
      { path: 'atc-detail/:id', component: ItemsDetailComponent },
      { path: 'profile/:id/:username', component: ProfileComponent}
    ]) 
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
