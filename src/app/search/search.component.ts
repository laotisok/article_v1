import { SharedService } from './../services/shared.service';
import { DocumentService } from './../services/document.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private documentService: DocumentService, private sharedService:SharedService) { }

  ngOnInit(): void {
  }

  filter(val){
    console.log("Filter: ",val);
    // this.documentService.getFilter(val).subscribe(response=>{
    //   console.log("Filer done: ",response); 
    // })

    this.sharedService.setFilter(val);
  }

  search(val){ 
    if(val["search"].length >= 3){
      this.sharedService.setSearch(val["search"]);
    }else{
      alert("Required 3 letters or more!")
    }
  }

}
