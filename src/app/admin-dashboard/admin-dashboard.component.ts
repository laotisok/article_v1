import { SharedService } from './../services/shared.service';
import { ActivatedRoute } from '@angular/router';
import { Document } from './../model/document';
import { DocumentService } from './../services/document.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { $ } from 'protractor';

/*export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  { position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na' },
  { position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg' },
  { position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al' },
  { position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si' },
  { position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P' },
  { position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S' },
  { position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl' },
  { position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar' },
  { position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K' },
  { position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca' },
];*/


/**
 * @title Table with pagination
 */
@Component({
  selector: 'admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  documents: Document[];
  displayedColumns: string[] = ['id', 'name', 'date', 'view', 'action'];
  // dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  dataSource : any;  
  classNameArr: any[]; 
  loading= false;

  constructor(private documentService:DocumentService, private route: ActivatedRoute, private sharedService:SharedService){
    this.classNameArr = [];
  }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() { 
    this.getAllDoc(); 
  }

  getAllDoc(){
    this.loading= true;
    var userId = 0;
    var category = [];
    this.route.paramMap.subscribe(params => {
      userId = +params.get('id');
    });  
    this.documentService.findAllMine(userId).then(
      document =>{
        console.log("category: ",document);  
        this.documents = document["data"];  
        this.dataSource =  new MatTableDataSource<Document>(this.documents);
        this.dataSource.paginator = this.paginator; 
        this.sharedService.setListCategory(document["categories"])
        this.loading= false;
      }
    ) 
    
    console.log("document2: ",this.documents);  
  }

  deleteDoc(docId){
    this.loading= true; 
    this.documentService.deleteDocument(docId).subscribe(
      document =>{
        console.log("Success: ",document)   
        this.classNameArr.push(docId);
        this.loading= false;
      }
    )
  }

  updateDoc(docId){
    console.log("docId: ",docId);
  }

  hasClass(className){  //Remove row from view
    var status = false;
    if(this.classNameArr.length >0){
      for(let cl of this.classNameArr ){ 
        if(cl == className){
          status = true;
        }
      }
    } 
    return status;
  }
}  