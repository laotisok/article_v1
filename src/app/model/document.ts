export class Document{
    id: number;
    doc_id: string; 
    doc_category_id: number;
    doc_category_name: string;
    user_id: number;
    doc_name: string;
    doc_content: string;
    doc_created_date: string;
    doc_status: string;  
    doc_type_name: string;
    doc_type_id: string;
    username: string;
    doc_view_amount: number;

    constructor( id: number, doc_id: string, doc_category_id: number, doc_category_name: string, user_id: number, doc_name: string, doc_content: string, doc_created_date: string, doc_status: string, doc_type_name: string, doc_type_id: string, username: string, doc_view_amount: number)
    {
        this.id               = id;
        this.doc_id           = doc_id; 
        this.doc_category_id  = doc_category_id;
        this.doc_category_name= doc_category_name
        this.user_id          = user_id;
        this.doc_name         = doc_name;
        this.doc_content      = doc_content;
        this.doc_created_date = doc_created_date;
        this.doc_status       = doc_status; 
        this.doc_type_name    = doc_type_name;
        this.doc_type_id      = doc_type_id;
        this.username         = username;
        this.doc_view_amount  = doc_view_amount;
    }
}