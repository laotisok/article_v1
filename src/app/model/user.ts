export class User{
    id: Number;
    user_role_id: Number;
    username: string;
    password: string;
    user_created_date: string;
    user_status: string;

    constructor(id: Number,
        user_role_id: Number,
        username: string,
        password: string,
        user_created_date: string,
        user_status: string){
            this.id = id;
            this.user_role_id = user_role_id;
            this.username = username;
            this.password = password;
            this.user_created_date = user_created_date;
            this.user_status = user_status;
        }
}