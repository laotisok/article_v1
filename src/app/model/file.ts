export class File{
    id: string;
    doc_id: string;
    doc_file_name: string;
    doc_file_path: string;
    doc_file_status: string;
    doc_file_created_date: string;

    constructor(id: string, doc_id: string, doc_file_name: string, doc_file_path: string, doc_file_status: string, doc_file_created_date: string){
        this.id = id;
        this.doc_id = doc_id;
        this.doc_file_name = doc_file_name;
        this.doc_file_path = doc_file_path;
        this.doc_file_status = doc_file_status;
        this.doc_file_created_date = doc_file_created_date;
    }
}