export class Category{
    id: Number;
    doc_user_id: Number;
    doc_category_id: string;
    doc_category_name: string;
    doc_category_status: Number;

    constructor(id: Number, doc_user_id: Number, doc_category_id: string, doc_category_name: string, doc_category_status: Number){
        this.id = id;
        this.doc_user_id = doc_user_id;
        this.doc_category_id = doc_category_id;
        this.doc_category_name = doc_category_name;
        this.doc_category_status = doc_category_status;
    }
}